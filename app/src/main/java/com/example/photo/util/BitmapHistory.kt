package com.example.photo.util

import android.graphics.Bitmap

enum class ImageFilters(val filterName:String){
    BLUR(BLUR_TEXT),
    NOISE(NOISE_TEXT),
    GRAYSCALE(GRAYSCALE_TEXT)
}

data class BitmapHistory(
    private val layers:MutableList<ImageFilters?> = mutableListOf(),
    var bitmap: Bitmap?
){
    fun addLayer(filter:ImageFilters){
        this.layers.add(filter)
    }

    fun deleteLayer(filter:ImageFilters){
        this.layers.remove(filter)
    }

    fun getLayers(): MutableList<ImageFilters?> {
        return layers
    }
}