package com.example.photo.util

data class Photo(
    var lighting:String = BRIGHT_TEXT,
    var area:String = AREA_TEXT,
    var gender:String = MALE_TEXT,
    var attributes: MutableList<String> = mutableListOf(),
    var filters:MutableList<String> = mutableListOf(),
    var maskType:Int = 1
) {
}