package com.example.photo.util

const val TABLE_NAME = "Photos"
const val TABLE_PHOTONAME = "Имя фотографии"
const val TABLE_LIGNTING = "Освещение"
const val TABLE_AREA = "Пространство"
const val TABLE_GENDER = "Пол"
const val TABLE_ATTRIBUTES = "Наличие дополнительных аттрибутов"
const val TABLE_FILTERS = "Фильтр внешних условий"
const val TABLE_NICKNAME = "Никнейм пользователя"
const val TABLE_MASKTYPE = "Тип маски"

const val BLUR_TEXT = "Блюр"
const val NOISE_TEXT = "Шум"
const val GRAYSCALE_TEXT = "Ч/Б"

const val BRIGHT_TEXT = "Светло"
const val AREA_TEXT = "Помещение"
const val MALE_TEXT = "Мужчина"