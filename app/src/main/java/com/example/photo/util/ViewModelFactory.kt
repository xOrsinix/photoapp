package com.example.photo.util

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.photo.screens.photoeditfragment.viewmodel.PhotoEditFragmentViewModel

class ViewModelFactory(
    private val context: Context,
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val viewModel = when(modelClass){
            PhotoEditFragmentViewModel::class.java -> {
                PhotoEditFragmentViewModel(context)
            }
            else -> {
                throw Exception()
            }
        }
        return viewModel as T
    }
}

fun Fragment.factory() = ViewModelFactory(this.requireActivity().applicationContext)