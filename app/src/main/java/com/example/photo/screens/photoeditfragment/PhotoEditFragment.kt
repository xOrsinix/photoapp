package com.example.photo.screens.photoeditfragment

import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.photo.MyApplication
import com.example.photo.screens.photoeditfragment.viewmodel.PhotoEditFragmentViewModel
import com.example.photo.R
import com.example.photo.State
import com.example.photo.databinding.FragmentPhotoEditBinding
import com.example.photo.room.Repositories
import com.example.photo.room.photos.PhotoDbEntity
import com.example.photo.screens.startfragment.StartFragment
import com.example.photo.util.BitmapHistory
import com.example.photo.util.ImageFilters
import com.example.photo.util.Photo
import com.example.photo.util.factory
import com.otaliastudios.cameraview.BitmapCallback
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.otaliastudios.cameraview.controls.Facing


class PhotoEditFragment : Fragment() {

    private val viewModel: PhotoEditFragmentViewModel by viewModels { factory() }

    private lateinit var binding:FragmentPhotoEditBinding

    private lateinit var application: MyApplication
    private lateinit var filename:String
    private var photoAttributes = Photo()
    private lateinit var exportDir:String

    private var counter = 0

    private var maskDrawable:Drawable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        application = requireActivity().application as MyApplication
        exportDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.absolutePath+"/${StartFragment.EXPORT_DIR}"
        binding = FragmentPhotoEditBinding.inflate(inflater,container,false)
        return binding.root
    }

    private fun getBitmapFromView(bitmap:Bitmap? = null,view: View): Bitmap {
        var outputBitmap = Bitmap.createBitmap(
            view.width, view.height, Bitmap.Config.ARGB_8888
        )
        if (bitmap!=null)
            outputBitmap = bitmap
        val canvas = Canvas(outputBitmap)
        view.draw(canvas)
        return outputBitmap
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupCamera()
        setUpButtons()
        setUpRadioButtons()
        setSideMenu()
        setUpObserver()
        binding.drawerLayout.openDrawer(GravityCompat.END)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupCamera(){
        binding.camera.setLifecycleOwner(viewLifecycleOwner)
        binding.camera.addCameraListener(object : CameraListener(){
            override fun onPictureTaken(result: PictureResult) {
                result.toBitmap(binding.constraintLayout.maxWidth,binding.constraintLayout.maxHeight,object: BitmapCallback{
                    override fun onBitmapReady(bitmap: Bitmap?) {
                        currentImageState.bitmap = bitmap!!
                        reset()
                    }
                })
            }
        })
    }

    private fun setUpButtons(){
        val anim = AnimationUtils.loadAnimation(requireContext(), R.anim.myalpha)
        anim.setAnimationListener(animListener)
        binding.apply {
            drawerLayout.addDrawerListener(drawerListener)
            btComplete.setOnClickListener {
                camera.takePictureSnapshot()
                constraintLayout.startAnimation(anim)
            }
            btCameraFlip.setOnClickListener {
                if (camera.facing == Facing.FRONT)
                    camera.facing = Facing.BACK
                else
                    camera.facing = Facing.FRONT
            }
            btSettings.setOnClickListener {
                drawerLayout.openDrawer(GravityCompat.END)
            }
            btPrev.setOnClickListener {
                findNavController().navigate(R.id.startFragment)
            }
            btMask.setOnClickListener {
                when(counter){
                    2->{
                        maskDrawable = ResourcesCompat.getDrawable(resources, R.drawable.red_oval,null)
                        photoAttributes.maskType =1
                    }
                    0->{
                        maskDrawable = ResourcesCompat.getDrawable(resources, R.drawable.golden_ration,null)
                        photoAttributes.maskType =2
                    }
                    1->{
                        maskDrawable = ResourcesCompat.getDrawable(resources, R.drawable.funny_mask,null)
                        photoAttributes.maskType =3
                    }
                }
                counter = (counter+1)%3
                mask.setImageDrawable(maskDrawable)
            }
        }
    }

    private fun reset() {
        photoAttributes.attributes = mutableListOf()
        photoAttributes.filters = mutableListOf()
        restoreImage(currentImageState)
    }

    private fun setUpRadioButtons() {
        binding.apply {
            rbLight.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbDark.isChecked = false
                    photoAttributes.lighting = rbLight.text.toString()
                }
            }
            rbDark.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbLight.isChecked = false
                    photoAttributes.lighting = rbDark.text.toString()
                }
            }
            rbRoom.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbOutdoor.isChecked = false
                    photoAttributes.area = rbRoom.text.toString()
                }
            }
            rbOutdoor.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbRoom.isChecked = false
                    photoAttributes.area = rbOutdoor.text.toString()
                }
            }
            rbMale.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbFemale.isChecked = false
                    photoAttributes.gender = rbMale.text.toString()
                }
            }
            rbFemale.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rbMale.isChecked = false
                    photoAttributes.gender = rbFemale.text.toString()
                }
            }
            rbLight.isChecked = true
            rbRoom.isChecked = true
            rbMale.isChecked = true
        }
    }

    private fun setUpObserver() {
        viewModel.photoEditFragmentModel.observe(viewLifecycleOwner){
            when (it.state){
                State.LOADING -> onLoadingState()
                State.LOADED -> onLoadedState(it.bitmap)
                else -> {}
            }
        }
    }

    private fun onLoadedState(bitmap: Bitmap?) {
        binding.apply {
            image.setImageBitmap(bitmap)

            Repositories.currentCount+=1
            filename = "${application.currentNickname}_foto_${Repositories.currentCount}"
            var bitmapToSave = getBitmapFromView(view = binding.image)
            bitmapToSave = getBitmapFromView(bitmapToSave,binding.maskLayout)
            viewModel.addPhotoToDb(photoToPhotoDbEntity(photoAttributes))
            viewModel.savePhoto(
                bitmapToSave,
                exportDir,
                "/$filename.jpeg"
            )
        }
        binding.btComplete.isEnabled = true
    }


    private fun onLoadingState() {

    }

    private val currentImageState:BitmapHistory = BitmapHistory(bitmap = null)

    private fun setSideMenu() {
        binding.cbBlur.setOnCheckedChangeListener{ _, isChecked ->
            if (isChecked){
                currentImageState.addLayer(ImageFilters.BLUR)
            }
            else{
                currentImageState.deleteLayer(ImageFilters.BLUR)
            }
        }
        binding.cbNoise.setOnCheckedChangeListener{ _, isChecked ->
            if (isChecked){
                currentImageState.addLayer(ImageFilters.NOISE)
            }
            else{
                currentImageState.deleteLayer(ImageFilters.NOISE)
            }
        }
        binding.cbBW.setOnCheckedChangeListener{ _, isChecked ->
            if (isChecked){
                currentImageState.addLayer(ImageFilters.GRAYSCALE)
            }
            else{
                currentImageState.deleteLayer(ImageFilters.GRAYSCALE)
            }
        }
    }

    private fun restoreImage(history: BitmapHistory){
        viewModel.restoreImage(history)
    }

    fun setButtonsClickable(state:Boolean){
        binding.btComplete.isClickable = state
        binding.btSettings.isClickable = state
        binding.btPrev.isClickable = state
        binding.btMask.isClickable = state
    }

    fun photoToPhotoDbEntity(photo:Photo):PhotoDbEntity{
        currentImageState.getLayers().forEach {
            photo.filters.add(it!!.filterName)
        }
        collectAttributes()
        return PhotoDbEntity(
            photoName = filename,
            lighting = photo.lighting,
            area = photo.area,
            gender = photo.gender,
            attributes = photo.attributes.joinToString(separator = "-"),
            filters = photo.filters.joinToString(separator = " - "),
            nickname = application.currentNickname,
            maskType = photo.maskType
        )
    }

    private fun collectAttributes() {
        binding.apply {
            if (cbHat.isChecked) {
                photoAttributes.attributes.add(cbHat.text.toString())
            }
            if (cbScarf.isChecked) {
                photoAttributes.attributes.add(cbScarf.text.toString())
            }
            if (cbGlasses.isChecked) {
                photoAttributes.attributes.add(cbGlasses.text.toString())
            }
            if (cbMask.isChecked) {
                photoAttributes.attributes.add(cbMask.text.toString())
            }
            if (cbMustache.isChecked) {
                photoAttributes.attributes.add(cbMustache.text.toString())
            }
            if (cbBeard.isChecked) {
                photoAttributes.attributes.add(cbBeard.text.toString())
            }
        }
    }

    val drawerListener = object : DrawerLayout.DrawerListener{
        override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

        }

        override fun onDrawerOpened(drawerView: View) {
            setButtonsClickable(false)
        }

        override fun onDrawerClosed(drawerView: View) {
            setButtonsClickable(true)
        }

        override fun onDrawerStateChanged(newState: Int) {

        }

    }

    val animListener = object : Animation.AnimationListener{
        override fun onAnimationStart(p0: Animation?) {
            binding.btComplete.isEnabled = false
        }

        override fun onAnimationEnd(p0: Animation?) {
        }

        override fun onAnimationRepeat(p0: Animation?) {

        }

    }

}