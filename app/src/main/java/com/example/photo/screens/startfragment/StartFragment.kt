package com.example.photo.screens.startfragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.photo.MyApplication
import com.example.photo.R
import com.example.photo.State
import com.example.photo.databinding.FragmentStartBinding
import com.example.photo.room.Repositories
import com.example.photo.screens.startfragment.viewmodel.StartFragmentViewModel
import java.io.File


class StartFragment : Fragment() {

    private lateinit var binding: FragmentStartBinding

    private lateinit var application: MyApplication

    private val viewModel:StartFragmentViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        application = requireActivity().application as MyApplication
        binding = FragmentStartBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btPhoto.setOnClickListener {
            if (application.currentNickname.isEmpty()) {
                binding.tvError.visibility = View.VISIBLE
                return@setOnClickListener
            }
            try {
                takeImage()
            }catch(e:Exception) {
                Log.e("TAG",e.message!!)
            }
        }
        binding.btDownload.setOnClickListener {
            if (Repositories.currentCount>0)
                download()
            else
                Toast.makeText(requireContext(),getString(R.string.low_amout_of_photos),Toast.LENGTH_SHORT).show()
        }
        binding.etNickname.setText(application.currentNickname)
        binding.etNickname.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                application.currentNickname = p0.toString()
            }

        })
        viewModel.count.observe(viewLifecycleOwner){
            binding.tvPhotoCount.setText(it.toString())
            Repositories.currentCount = it
        }
        viewModel.downLoadState.observe(viewLifecycleOwner){
            when(it) {
                State.LOADING -> onLoadingState()
                State.ERROR -> onErrorState()
                State.LOADED -> OnLoadedState()
            }
        }
        viewModel.getCount()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun OnLoadedState() {
        binding.apply {
            progressBar.visibility = View.INVISIBLE
            setInterfaceVisibility(View.VISIBLE)
        }
        Toast.makeText(requireContext(),getString(R.string.data_destination),Toast.LENGTH_LONG).show()
    }

    private fun onErrorState() {
        fail()
        setInterfaceVisibility(View.VISIBLE)
    }

    private fun onLoadingState() {
        binding.apply {
            progressBar.visibility = View.VISIBLE
            setInterfaceVisibility(View.INVISIBLE)
        }
    }

    private fun setInterfaceVisibility(visibility:Int){
        binding.apply {
            tvNickname.visibility = visibility
            etNickname.visibility = visibility
            tvPhotoCount.visibility = visibility
            tvPhotoCountText.visibility = visibility
            tvDownloadHint.visibility = visibility
            btDownload.visibility = visibility
            btPhoto.visibility = visibility
        }
    }

    private fun download() {
        val xslAndZipFilePath = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val xlsFile = File(xslAndZipFilePath, PHOTOS_TABLE)
        xlsFile.createNewFile()
        if (xlsFile.exists()){
            viewModel.createXls(xlsFile,xslAndZipFilePath!!)
        }
        else{
            fail()
        }
    }

    private fun fail(){
        Toast.makeText(requireContext(),getString(R.string.error_download),Toast.LENGTH_SHORT).show()
    }

    fun hasCameraPermission() = ContextCompat.checkSelfPermission(requireActivity().applicationContext,
        Manifest.permission.CAMERA)
    fun hasExternalStoragePermission() = ContextCompat.checkSelfPermission(requireActivity().applicationContext,
        Manifest.permission.WRITE_EXTERNAL_STORAGE)

    val requestMultiplePermissionsLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ resultMap ->
        var permissionGranted = true
        resultMap.forEach{
            permissionGranted = it.value && permissionGranted
        }
        if (permissionGranted)
            findNavController().navigate(R.id.action_startFragment_to_photoEditFragment)
        else {
            binding.tvError.text = getString(R.string.error_permissions)
            binding.tvError.visibility = View.VISIBLE
        }
    }

    fun takeImage(){
        if (hasCameraPermission() == PackageManager.PERMISSION_GRANTED && hasExternalStoragePermission() == PackageManager.PERMISSION_GRANTED){
            findNavController().navigate(R.id.photoEditFragment)
        }
        else{
            requestMultiplePermissionsLauncher.launch(arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ))
        }
    }


    companion object{
        const val PHOTOS_TABLE = "Photos.xls"
        const val PHOTOS_ZIP = "photos.zip"
        const val EXPORT_DIR = "Export"
    }
}