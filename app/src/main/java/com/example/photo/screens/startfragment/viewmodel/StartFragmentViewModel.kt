package com.example.photo.screens.startfragment.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.photo.State
import com.example.photo.room.Repositories
import com.example.photo.screens.startfragment.StartFragment
import com.example.photo.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.lingala.zip4j.ZipFile
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class StartFragmentViewModel:ViewModel() {

    val count = MutableLiveData<Int>()

    val downLoadState = MutableLiveData<State>(State.NULL)

    fun getCount(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                count.postValue(Repositories.photosRepository.getCount())
            }
        }
    }

    fun createXls(xsl: File,zipPath:File){
        downLoadState.postValue(State.LOADING)
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                flow {
                    emit(Repositories.photosRepository.getAll())
                }
                    .flowOn(Dispatchers.IO)
                    .catch {
                        downLoadState.postValue(State.ERROR)
                    }
                    .collect{ photoList ->
                        flow{
                            val workbook = HSSFWorkbook()
                            val sheet = workbook.createSheet(TABLE_NAME)
                            (0..7).map {
                                sheet.setColumnWidth(it,15*400)
                            }
                            var row = sheet.createRow(0)
                            val titles = listOf(
                                TABLE_PHOTONAME, TABLE_LIGNTING, TABLE_AREA, TABLE_GENDER
                                , TABLE_ATTRIBUTES, TABLE_FILTERS
                                , TABLE_NICKNAME, TABLE_MASKTYPE)
                            for (ind in titles.indices){
                                val cell = row.createCell(ind)
                                cell.setCellValue(titles[ind])
                            }
                            for (ind in photoList.indices){
                                row = sheet.createRow(ind+1)
                                var cell = row.createCell(0)
                                cell.setCellValue(photoList[ind].photoName)

                                cell = row.createCell(1)
                                cell.setCellValue(photoList[ind].lighting)

                                cell = row.createCell(2)
                                cell.setCellValue(photoList[ind].area)

                                cell = row.createCell(3)
                                cell.setCellValue(photoList[ind].gender)

                                cell = row.createCell(4)
                                cell.setCellValue(photoList[ind].attributes)

                                cell = row.createCell(5)
                                cell.setCellValue(photoList[ind].filters)

                                cell = row.createCell(6)
                                cell.setCellValue(photoList[ind].nickname)

                                cell = row.createCell(7)
                                cell.setCellValue(photoList[ind].maskType.toString())
                            }
                            val fileOutputStream = FileOutputStream(xsl)
                            workbook.write(fileOutputStream)
                            fileOutputStream.close()
                            emit(true)
                        }
                            .catch {
                                downLoadState.postValue(State.ERROR)
                            }
                            .flowOn(Dispatchers.IO)
                            .collect{
                                if (it)
                                    createZip(zipPath)
                            }
                    }
            }
        }
    }

    private fun createZip(path:File){
        viewModelScope.launch {
            withContext(Dispatchers.Default){
                flow {
                    val photosPath = File(path.absolutePath+"/${StartFragment.EXPORT_DIR}")
                    val zipFile = ZipFile(path.absolutePath+"/${StartFragment.PHOTOS_ZIP}")
                    photosPath.list().forEach {
                        zipFile.addFile(File(photosPath,it))
                    }
                    emit(true)
                }
                    .catch {
                        downLoadState.postValue(State.ERROR)
                    }
                    .flowOn(Dispatchers.Default)
                    .collect{
                        downLoadState.postValue(State.LOADED)
                    }
            }
        }

    }

}