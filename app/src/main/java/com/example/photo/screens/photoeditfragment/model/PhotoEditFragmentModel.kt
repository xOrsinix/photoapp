package com.example.photo

import android.graphics.Bitmap

enum class State{
    NULL,
    LOADING,
    LOADED,
    ERROR
}

data class PhotoEditFragmentModel(
    val state:State = State.NULL,
    val bitmap:Bitmap? = null
) {
}