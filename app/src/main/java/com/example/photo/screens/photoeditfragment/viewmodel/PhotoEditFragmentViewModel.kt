package com.example.photo.screens.photoeditfragment.viewmodel

import android.content.Context
import android.graphics.*
import android.util.Log
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.photo.PhotoEditFragmentModel
import com.example.photo.State
import com.example.photo.room.Repositories
import com.example.photo.room.photos.PhotoDbEntity
import com.example.photo.util.BitmapHistory
import com.example.photo.util.ImageFilters
import jp.wasabeef.blurry.Blurry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.*

class PhotoEditFragmentViewModel(private val applicationContext: Context):ViewModel() {

    val photoEditFragmentModel = MutableLiveData<PhotoEditFragmentModel>()

    init {
        photoEditFragmentModel.value = PhotoEditFragmentModel()
    }

    fun addPhotoToDb(photoDbEntity: PhotoDbEntity){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                Repositories.photosRepository.insertPhoto(photoDbEntity)
            }
        }
    }

    fun savePhoto(bitmap: Bitmap, path:String,fileName:String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val tmpFile = File(path)
                if (!tmpFile.exists())
                    tmpFile.mkdir()
                try{
                    val out = FileOutputStream(path+fileName)
                    bitmap.compress(Bitmap.CompressFormat.JPEG,75,out)
                    out.close()
                }catch (e:Exception){
                    Log.e("FILE_SAVE", e.message.toString())
                }
            }
        }
    }

    fun grayscaleEffect(source: Bitmap):Flow<Bitmap> {
        return flow {
            val outputBitmap = Bitmap.createBitmap(source.width, source.height, source.config)
            val canvas = Canvas(outputBitmap)
            val paint = Paint()
            paint.colorFilter = ColorMatrixColorFilter(
                ColorMatrix(
                    floatArrayOf(
                        0f, 1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 0f, 0f,
                        0f, 1f, 0f, 1f, 0f
                    )
                )
            )
            canvas.drawBitmap(source, 0f, 0f, paint)
            emit(outputBitmap)
        }
            .flowOn(Dispatchers.Default)
    }

    private fun fleaEffect(source: Bitmap):Flow<Bitmap>{
        return flow {
                    val width = source.width
                    val height = source.height
                    val pixels = IntArray(width * height)
                    source.getPixels(pixels, 0, width, 0, 0, width, height)
                    val random = Random()
                    var index = 0
                    for (y in 0 until height) {
                        for (x in 0 until width) {
                            index = y * width + x
                            val randColor: Int = Color.rgb(
                                random.nextInt(255),
                                random.nextInt(255), random.nextInt(255)
                            )
                            pixels[index] = pixels[index] or randColor
                        }
                    }
                    val bmOut = Bitmap.createBitmap(width, height, source.config)
                    bmOut.setPixels(pixels, 0, width, 0, 0, width, height)
                    emit(bmOut)
                }
                    .flowOn(Dispatchers.Default)
    }

    private fun blurEffect(source: Bitmap):Flow<Bitmap>{
       return flow {
           val tmpImageView = ImageView(applicationContext)
           Blurry.with(applicationContext)
               .radius(5)
               .sampling(4)
               .from(source)
               .into(tmpImageView)
           emit(tmpImageView.drawable.toBitmap())
       }
           .flowOn(Dispatchers.IO)
    }

    fun restoreImage(bitmapHistory: BitmapHistory){
        postModel(null, State.LOADING)
        var currentBitmap = bitmapHistory.bitmap
        viewModelScope.launch {
            withContext(Dispatchers.Default){
                bitmapHistory.getLayers().forEach {
                    when(it){
                        ImageFilters.BLUR -> {
                            blurEffect(currentBitmap!!)
                                .collect{
                                    currentBitmap = it
                                }
                        }
                        ImageFilters.GRAYSCALE -> {
                            grayscaleEffect(currentBitmap!!)
                                .collect{
                                    currentBitmap = it
                                }
                        }
                        ImageFilters.NOISE -> {
                            fleaEffect(currentBitmap!!)
                                .collect{
                                    currentBitmap = it
                                }
                        }
                    }
                }
                Log.e("RESTORE_IMAGE","CHECK")
                postModel(currentBitmap, State.LOADED)
            }
        }
    }

    private fun postModel(bitmap: Bitmap?,state: State){
        photoEditFragmentModel.postValue(
            PhotoEditFragmentModel(
            state = state,
            bitmap = bitmap
        )
        )
    }
}