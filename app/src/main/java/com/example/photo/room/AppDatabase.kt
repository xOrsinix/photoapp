package com.example.photo.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.photo.room.photos.PhotoDao
import com.example.photo.room.photos.PhotoDbEntity

@Database(
    version = 1,
    entities = [
        PhotoDbEntity::class
    ]
)
abstract class AppDatabase:RoomDatabase() {

    abstract fun getPhotoDao(): PhotoDao

}