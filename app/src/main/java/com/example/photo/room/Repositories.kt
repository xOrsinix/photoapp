package com.example.photo.room

import android.content.Context
import androidx.room.Room
import com.example.photo.room.photos.PhotosRepository

object Repositories {

    private lateinit var applicationContext:Context

    var currentCount = 0

    fun setContext(context: Context){
        applicationContext = context
    }

    private val database:AppDatabase by lazy<AppDatabase>{
        Room.databaseBuilder(applicationContext,AppDatabase::class.java,"database.db")
            .build()
    }

    val photosRepository:PhotosRepository by lazy{
        PhotosRepository(database.getPhotoDao())
    }

}