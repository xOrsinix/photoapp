package com.example.photo.room.photos

class PhotosRepository(private val photoDao: PhotoDao) {

    suspend fun insertPhoto(photoDbEntity: PhotoDbEntity){
        photoDao.insertPhoto(photoDbEntity)
    }

    suspend fun getCount():Int {
        return photoDao.getCount()
    }

    suspend fun getAll():List<PhotoDbEntity>{
        return photoDao.getAll()
    }

}