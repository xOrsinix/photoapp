package com.example.photo.room.photos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface PhotoDao {

    @Insert
    suspend fun insertPhoto(photo:PhotoDbEntity)

    @Query("SELECT COUNT(photo_name) FROM photos")
    suspend fun getCount(): Int

    @Query("SELECT * FROM photos")
    suspend fun getAll(): MutableList<PhotoDbEntity>

}