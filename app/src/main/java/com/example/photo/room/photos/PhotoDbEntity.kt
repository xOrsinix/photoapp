package com.example.photo.room.photos

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "photos"
)
data class PhotoDbEntity(
    @PrimaryKey() @ColumnInfo(name = "photo_name")val photoName:String,
    val lighting:String,
    val area:String,
    val gender:String,
    val attributes:String,
    val filters:String,
    val nickname:String,
    @ColumnInfo(name = "mask_type")val maskType:Int
) {
}